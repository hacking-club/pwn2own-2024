/*
 * This ESP32 code is created by esp32io.com
 *
 * This ESP32 code is released in the public domain
 *
 * For more detail (instruction and wiring diagram), visit https://esp32io.com/tutorials/esp32-led-blink
 */

const int dataPin = 27;  // Pin connected to the onboard LED
const int dataPin2 = 25;  // Pin connected to the onboard LED
const int moleLightPin = 32;
const int moleLightPin2 = 33;
const int moleReadPin = 13;
const int moleReadPin2 = 14;
const int startPin = 26;
unsigned int seed = 42;

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin GPIO18 as an output.
  Serial.begin(9600);  
//  Serial.println("FUCK");
//  return;
  pinMode(dataPin, OUTPUT);
  pinMode(dataPin2, OUTPUT);
  pinMode(startPin, OUTPUT);
  pinMode(moleLightPin, OUTPUT);
  pinMode(moleLightPin2, OUTPUT);
  pinMode(moleReadPin, INPUT);

  digitalWrite(moleLightPin, LOW);
  digitalWrite(moleLightPin2, LOW);
  digitalWrite(startPin, LOW);
  digitalWrite(dataPin2, HIGH);  // Turn off LED after the delay
    digitalWrite(dataPin, HIGH);
  int data[] = READFROMBULB();

  Serial.println("Finished reading daata");
  // TODO: get data from serial

  // Display LED data
  preprocess(data[0], data[1], data[2]);

  Serial.println("Preprocess DONE");

  
  // Give go signal
  digitalWrite(startPin, HIGH);

  int score = 0;
  // Start game

  for(int i = 0; i < 25; i++){
    Serial.print("Round: ");
    Serial.println(i);
    int turn = getRandom();
//    Serial.println(turn);
    Serial.print("Score: ");
    Serial.println(score);
    if(turn == 0){
//      Serial.println("MOLE1");
      digitalWrite(moleLightPin, HIGH);
      delay(50);
      //TODO: READ action here
      for(int i =0 ; i < 5;i++){
        int r1 = digitalRead(moleReadPin);
        int r2 = digitalRead(moleReadPin2);
        if(r1 == r2){
            continue;
        }
        if(r1 == HIGH){
           score++;
            break;
        }
      }
      digitalWrite(moleLightPin, LOW);
    }
    else{
//      Serial.println("MOLE2");
      digitalWrite(moleLightPin2, HIGH);
      delay(100);
      //TODO: READ action here
      for(int i =0 ; i < 5;i++){
        int r1 = digitalRead(moleReadPin);
        int r2 = digitalRead(moleReadPin2);
        if(r1 == r2){
            continue;
        }
        if(r2 == HIGH){
          score++;
          break;
        }
      }
      digitalWrite(moleLightPin2, LOW);
    }
    delay(1000 + getRandom() * 500);
  }
  Serial.print("Final Score is: ");
  Serial.println(score);
  
}

void preprocess(signed char a, signed char b, signed char c){
  int pp[] = {a, b, c};
  signed char acc = 0;
  for(int i = 0; i < 3; i++){
    Serial.println(pp[i]);
    if(pp[i] >= 97 && pp[i] <= 122)
      pp[i] = pp[i] - 37; 
    acc += pp[i];
    Serial.println(acc);
  }
  seed = acc;  
  Serial.println((int) seed);
  
}    

int getRandom(){
  int a = 70;
  int c = 69;
  seed = a * seed + c;
  Serial.print("SEED: ");
  Serial.println(seed);
  return (( seed & (1 << 20)) > 0);  
}


