## Whack-a-mole

#### Objective

Get 25 points in one game

#### Notes:
- Game starts once the GREEN led turns on
- Hit the left side when left light turns on and right side if right side turns on
- Source code present in the ESP can be found in `src.cpp`
