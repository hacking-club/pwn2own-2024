# pwn2own-2024

This is the official reference for Pwn2Own 2024.

# Devil in the details
You have been given access to the secret servers of a corporate overloard, but they have everything behind a password! 

Note: Do **not** connect to the mother board. Only power it on, and flash any communication to the daughter board. The starter code for the daughter board can be found [./devil-in-the-details-starter.cpp](here).

# Break the ESP

## Description

This is a simple challenge to get familiar with Hardware CTF on ESP32. The challenge is to get the flag from the ESP32. The final flag is in the format `pwn2own{flag}`. To Start with Challenge, Plug in the ESP32 and You are good to go.

## Things to Keep in Mind

1. Do not flash any other firmware or code on the ESP32.
2. In case of any issues, please contact the organizers.
3. You can use Your Own Hotspot whenever it is needed.

# Router

Connect to the HackMe WiFi network. It will be considered Pwned if you gain root access to the router. Please try not to brick the router.

You are not allowed to touch the router.

# CubeSprint

Dive into the dynamic world of Cube Sprint, where speed, skill, and secrets collide! Race your cube through a maze of obstacles, aiming to become the prime contender for the top leaderboard spot.

# Bluecipher

A simple BLE challenge to test your Bluetooth skills.
Connect to 94:b5:55:2d:3a:20 at the handle 0x0052 to view the challenge. For those who don't know what that means, use the below command,
```
gatttool -b 94:b5:55:2d:3a:20 --char-write-req -a 0x0052 -n 0001
```

To submit your flag, connect to 94:b5:55:2d:3a:20 at the handle 0x002e. FOr those who don't know how to do that, use the below command,
```
gatttool -b 94:b5:55:2d:3a:20 --char-write-req -a 0x002e -n $(echo -n "THE FLAG"|xxd -ps)
```

After submitting run,
```
gatttool -b 94:b5:55:2d:3a:20 --char-read -a 0x002a|awk -F':' '{print $2}'|tr -d ' '|xxd -r -p;printf '\n'
```
to check if your flag was right. Your score should be 1 after a correct submission.

Due to this challenge not being able to support multiple users at once, please contact one of the club members to ensure that the score is reset to 0 before trying the challenge.
